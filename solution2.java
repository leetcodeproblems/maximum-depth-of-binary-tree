/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
 
class Solution {
    static int maxCount;
    static int[] path;
    public int maxDepth(TreeNode root) {
        maxCount=0;
        path = new int[3];
        helperRecur(root, 0);
        return maxCount;
    }
    
    void helperRecur(TreeNode t, int pathLen) {
        if (t == null)
            return;
        if (pathLen >= path.length)
            resizeArray();
        path[pathLen] = t.val;
        pathLen++;

        if (t.left == null && t.right == null) {
            maxCount=Math.max(maxCount,pathLen);
        } else {
            helperRecur(t.left, pathLen);
            helperRecur(t.right, pathLen);
        }

    }
    
    void resizeArray() {
        path = Arrays.copyOf(path, path.length * 2);
    }


}
