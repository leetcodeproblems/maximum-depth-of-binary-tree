/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
//BFS with 2 Queues
class Solution {
    public int maxDepth(TreeNode root) {
        
        int level=0;
        if(root==null) return level;
        
        Queue<TreeNode> q = new LinkedList<>();
        Queue<TreeNode> p = new LinkedList<>();
        
        q.add(root);
        
        while(!q.isEmpty()){
            //front
            TreeNode front = q.remove();
            
            //neighbors
            if(front.left != null) p.add(front.left);
            if(front.right != null) p.add(front.right);
            
            //level over
            if(q.isEmpty()){
                level++;
                q=p;
                p=new LinkedList<>();
            }
            
        }
        return level;
    }
}
